VERSION 5.00
Begin VB.Form RegisterLibraries 
   Caption         =   "RegisterLibraries"
   ClientHeight    =   3030
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   4560
   LinkTopic       =   "Form1"
   ScaleHeight     =   3030
   ScaleWidth      =   4560
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
End
Attribute VB_Name = "RegisterLibraries"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Private Sub Form_Load()

Call InstallResources
Call RegisterAppLibraries

Unload Me

End Sub


Public Sub InstallResources()

Shell "Resources/SDK.exe", 2

Sleep (5000)

Shell "Resources/Setup.exe", 2

Sleep 7500

Shell "Resources/InstMsiA.exe", 2

Sleep 2500

Shell "Resources/InstMsiW.exe", 2

Sleep 2500

End Sub

Public Sub RegisterAppLibraries()

OBJ = Shell("WScript Hidden.vbs RegisterAppLibraries.bat", 2)

End Sub
