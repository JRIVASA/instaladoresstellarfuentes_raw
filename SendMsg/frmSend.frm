VERSION 5.00
Begin VB.Form frmSend 
   BackColor       =   &H00000000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Send Data"
   ClientHeight    =   2160
   ClientLeft      =   3090
   ClientTop       =   3225
   ClientWidth     =   5190
   Icon            =   "frmSend.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2160
   ScaleWidth      =   5190
   Begin VB.Frame Frame3 
      BackColor       =   &H00000000&
      Caption         =   "Communicate with the Receiving Application"
      ForeColor       =   &H00FFFFFF&
      Height          =   1935
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   4935
      Begin VB.Frame Frame4 
         BackColor       =   &H00000000&
         Caption         =   "Communicate via its Window"
         ForeColor       =   &H00FFFFFF&
         Height          =   855
         Left            =   120
         TabIndex        =   4
         Top             =   960
         Width           =   4575
         Begin VB.CommandButton cmdSendData 
            Caption         =   "Send the Above String"
            Height          =   375
            Left            =   120
            TabIndex        =   1
            Top             =   360
            Width           =   1815
         End
      End
      Begin VB.TextBox txtString 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   120
         TabIndex        =   0
         Top             =   600
         Width           =   4575
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Enter the String to Send"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   360
         Width           =   2175
      End
   End
End
Attribute VB_Name = "frmSend"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Const WM_COPYDATA = &H4A

Private Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)

Private Declare Function SendMessageTimeout Lib "user32" Alias "SendMessageTimeoutA" (ByVal hwnd As Long, ByVal msg As Long, ByVal wParam As Long, ByVal lParam As Long, ByVal fuFlags As Long, ByVal uTimeout As Long, lpdwResult As Long) As Long

Private Declare Function SendMessage Lib "user32" Alias _
   "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal _
   wParam As Long, lParam As Any) As Long
   
Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" _
    (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
    
Private Type COPYDATASTRUCT
    dwData As Long   ' Use this to identify your message
    cbData As Long   ' Number of bytes to be transferred
    lpData As Long   ' Address of data
End Type

Private Sub Form_Load()

    Dim sString As String
    Dim RecAppWindowName As String
    
    Dim lHwnd   As Long
    Dim CDS     As COPYDATASTRUCT
    Dim Buf(1 To 255) As Byte
    
    Dim RawParams As String: RawParams = Command$()
    'Dim tmp As Variant: tmp = Command()
        
    If (RawParams <> "") Then
    
        'Dim Params() As String: Params = Split(RawParams, "|-|")
        'Dim Param As String
    
        'For Each Param In Params
            'No hay tiempo...
        'Next
    
        sString = RawParams
        RecAppWindowName = "StellarInstallerExternalProgressID"
        
        'MsgBox Params 'TestParams
        'Unload Me
        'Exit Sub
        
    Else
        Unload Me
        Exit Sub
    End If
        
    lHwnd = FindWindow(vbNullString, RecAppWindowName)
    
    Call CopyMemory(Buf(1), ByVal sString, Len(sString))
    
    With CDS
        .dwData = 3
        .cbData = Len(sString) + 1
        .lpData = VarPtr(Buf(1))
    End With
    
    'lHwnd = 11080502
    
    Call SendMessage(lHwnd, WM_COPYDATA, Me.hwnd, CDS)
    
    Unload Me

End Sub
