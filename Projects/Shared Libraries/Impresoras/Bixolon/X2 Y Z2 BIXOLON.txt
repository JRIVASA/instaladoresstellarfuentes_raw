SI DESEA QUE SE IMPRIMAN EL REPORTE X2 Y Z2 RESPECTIVAMENTE ADICIONAL
AL REPORTE X Y Z, ENTONCES RENOMBRE LOS ARCHIVOS DE MANERA CORRESPONDIENTE:

R_X2_BAK.dat -> R_X2.dat

R_Z2_BAK.dat -> R_Z2.dat

Si no tiene inter�s en imprimir dichos documentos, no haga cambio en el nombre de los archivos para que el sistema no los encuentre y no los imprima.