REVIEW THE DEVICES LIST AND SELECT THE APPROPIRIATE VALUE FROM THE
CORRESPONDING SecureDeviceID Tag, DEPENDING ON YOUR DEVICE MODEL,
REQUIRED HARDWARE CAPABILITIES AND PAYMENT PROCESSOR.

FOR EXAMPLE, FOR THESE KNOWN VALUES:
* DEVICE BRAND / MODEL YOU WOULD BE USING / HAVE ALREADY INSTALLED IS: Verifone Vx805
* YOUR PAYMENT PROCESSOR FOR TRANSACTIONS IS: Mercury
* DEVICE has a USB Cable / Rs232 cable for connection to the computer

THEN YOU SHOULD HAVE SCROLLED THE DEVICE LIST AND TAKEN THE VALUE FOR
SecureDevice45 > SecureDeviceID45 > "VX805XPI_MERCURY_E2E"
THIS IS THE VALUE YOU NEED TO SET FOR THIS PARAMETER.
DEVICE INFO THAT MATCHES YOUR EXAMPLE KNOWN VALUES (HOW IT LOOKS):

  <SecureDevice45>
    <Description45>Verifone Vx805 Pinpad running XPI with Mercury E2E Encryption</Description45>
    <Interface45>RS-232 or USB/VCom</Interface45>
    <SecureDeviceID45>VX805XPI_MERCURY_E2E</SecureDeviceID45>
    <NumPadTypes45>1</NumPadTypes45>
    <SecureDevice45PadType1>VX805</SecureDevice45PadType1>
  </SecureDevice45>
  
THE NEXT PARAMETER YOU WILL ALSO BE LOOKING FOR TO SET IS SECURE DEVICE PAD TYPE, SO MAKE SURE
TO EXTRACT ALSO THE VALUE FOR PadType1 FOR THE MATCHING SECURE DEVICE, WHICH IN THE PREVIOUS CASE WOULD BE:

"VX805" FROM

<SecureDevice45PadType1>VX805</SecureDevice45PadType1>

IF NOT SURE, CONTACT TECHNICAL SUPPORT.

THE DEVICE LIST:

<Devices>
  <NumSecureDevices>100</NumSecureDevices>
  <SecureDevice1>
    <Description1>Datacap PDC</Description1>
    <Interface1>RS-232 or USB/VCom</Interface1>
    <SecureDeviceID1>PDC</SecureDeviceID1>
    <NumPadTypes1>2</NumPadTypes1>
    <SecureDevice1PadType1>VFI1000</SecureDevice1PadType1>
    <SecureDevice1PadType2>None</SecureDevice1PadType2>
  </SecureDevice1>
  <SecureDevice2>
    <Description2>Datacap PDC2</Description2>
    <Interface2>RS-232 or USB/VCom</Interface2>
    <SecureDeviceID2>PDC2</SecureDeviceID2>
    <NumPadTypes2>2</NumPadTypes2>
    <SecureDevice2PadType1>VFI1000</SecureDevice2PadType1>
    <SecureDevice2PadType2>None</SecureDevice2PadType2>
  </SecureDevice2>
  <SecureDevice3>
    <Description3>Verifone Vx810 PIN pad running XPI</Description3>
    <Interface3>RS-232 or USB/VCom</Interface3>
    <SecureDeviceID3>VX810XPI</SecureDeviceID3>
    <NumPadTypes3>1</NumPadTypes3>
    <SecureDevice3PadType1>VX810</SecureDevice3PadType1>
  </SecureDevice3>
  <SecureDevice4>
    <Description4>UIC 795 PIN pad</Description4>
    <Interface4>RS-232 or USB/VCom</Interface4>
    <SecureDeviceID4>UIC795</SecureDeviceID4>
    <NumPadTypes4>1</NumPadTypes4>
    <SecureDevice4PadType1>UIC795</SecureDevice4PadType1>
  </SecureDevice4>
  <SecureDevice5>
    <Description5>ViVoPay 4500m contactless with MSR</Description5>
    <Interface5>RS-232 or USB/VCom</Interface5>
    <SecureDeviceID5>VIVO4500M</SecureDeviceID5>
    <NumPadTypes5>1</NumPadTypes5>
    <SecureDevice5PadType1>None</SecureDevice5PadType1>
  </SecureDevice5>
  <SecureDevice6>
    <Description6>MagTek SureSwipe MSR</Description6>
    <Interface6>RS-232 or USB/VCom</Interface6>
    <SecureDeviceID6>MTSURESWIPEVCOM</SecureDeviceID6>
    <NumPadTypes6>1</NumPadTypes6>
    <SecureDevice6PadType1>None</SecureDevice6PadType1>
  </SecureDevice6>
  <SecureDevice7>
    <Description7>MagTek MINI MSR</Description7>
    <Interface7>RS-232 or USB/VCom</Interface7>
    <SecureDeviceID7>MTMINIMSRVCOM</SecureDeviceID7>
    <NumPadTypes7>1</NumPadTypes7>
    <SecureDevice7PadType1>None</SecureDevice7PadType1>
  </SecureDevice7>
  <SecureDevice8>
    <Description8>MagTek MINI MSR</Description8>
    <Interface8>RS-232</Interface8>
    <SecureDeviceID8>MTMINIMSR</SecureDeviceID8>
    <NumPadTypes8>1</NumPadTypes8>
    <SecureDevice8PadType1>None</SecureDevice8PadType1>
  </SecureDevice8>
  <SecureDevice9>
    <Description9>MagTek SureSwipe MSR</Description9>
    <Interface9>USB/HID</Interface9>
    <SecureDeviceID9>MTSURESWIPEHID</SecureDeviceID9>
    <NumPadTypes9>1</NumPadTypes9>
    <SecureDevice9PadType1>None</SecureDevice9PadType1>
  </SecureDevice9>
  <SecureDevice10>
    <Description10>MagTek MINI MSR</Description10>
    <Interface10>USB/HID</Interface10>
    <SecureDeviceID10>MTMINIMSRHID</SecureDeviceID10>
    <NumPadTypes10>1</NumPadTypes10>
    <SecureDevice10PadType1>None</SecureDevice10PadType1>
  </SecureDevice10>
  <SecureDevice11>
    <Description11>IDTech MSR No Encrypt</Description11>
    <Interface11>USB/HID</Interface11>
    <SecureDeviceID11>IDTMSRHID</SecureDeviceID11>
    <NumPadTypes11>1</NumPadTypes11>
    <SecureDevice11PadType1>None</SecureDevice11PadType1>
  </SecureDevice11>
  <SecureDevice12>
    <Description12>IDTech POSIX MSR no Encrypt</Description12>
    <Interface12>USB/HID</Interface12>
    <SecureDeviceID12>IDTPOSIXMSRHID</SecureDeviceID12>
    <NumPadTypes12>1</NumPadTypes12>
    <SecureDevice12PadType1>None</SecureDevice12PadType1>
  </SecureDevice12>
  <SecureDevice13>
    <Description13>IDTech MSR Encrypted</Description13>
    <Interface13>USB/HID</Interface13>
    <SecureDeviceID13>IDTSECUREMAGHID</SecureDeviceID13>
    <NumPadTypes13>1</NumPadTypes13>
    <SecureDevice13PadType1>None</SecureDevice13PadType1>
  </SecureDevice13>
  <SecureDevice14>
    <Description14>IDTech Augusta</Description14>
    <Interface14>USB/HID</Interface14>
    <SecureDeviceID14>IDTAUGUSTAHID</SecureDeviceID14>
    <NumPadTypes14>1</NumPadTypes14>
    <SecureDevice14PadType1>None</SecureDevice14PadType1>
  </SecureDevice14>
  <SecureDevice15>
    <Description15>IDTech SecureHead</Description15>
    <Interface15>USB/HID</Interface15>
    <SecureDeviceID15>IDTSECUREHEADHID</SecureDeviceID15>
    <NumPadTypes15>1</NumPadTypes15>
    <SecureDevice15PadType1>None</SecureDevice15PadType1>
  </SecureDevice15>
  <SecureDevice16>
    <Description16>IDTech Augusta with Mercury E2E Encryption</Description16>
    <Interface16>USB/HID</Interface16>
    <SecureDeviceID16>IDTAUGUSTAHID_MERCURY_E2E</SecureDeviceID16>
    <NumPadTypes16>1</NumPadTypes16>
    <SecureDevice16PadType1>None</SecureDevice16PadType1>
  </SecureDevice16>
  <SecureDevice17>
    <Description17>IDTech SecureHead with Mercury E2E Encryption</Description17>
    <Interface17>USB/HID</Interface17>
    <SecureDeviceID17>IDTSECUREHEADHID_MERCURY_E2E</SecureDeviceID17>
    <NumPadTypes17>1</NumPadTypes17>
    <SecureDevice17PadType1>None</SecureDevice17PadType1>
  </SecureDevice17>
  <SecureDevice18>
    <Description18>IDTech Augusta with Heartland E2E Encryption</Description18>
    <Interface18>USB/HID</Interface18>
    <SecureDeviceID18>IDTAUGUSTAHID_HEARTLAND_E2E</SecureDeviceID18>
    <NumPadTypes18>1</NumPadTypes18>
    <SecureDevice18PadType1>None</SecureDevice18PadType1>
  </SecureDevice18>
  <SecureDevice19>
    <Description19>IDTech SecureHead with Heartland E2E Encryption</Description19>
    <Interface19>USB/HID</Interface19>
    <SecureDeviceID19>IDTSECUREHEADHID_HEARTLAND_E2E</SecureDeviceID19>
    <NumPadTypes19>1</NumPadTypes19>
    <SecureDevice19PadType1>None</SecureDevice19PadType1>
  </SecureDevice19>
  <SecureDevice20>
    <Description20>IDTech Augusta with Monetary E2E Encryption</Description20>
    <Interface20>USB/HID</Interface20>
    <SecureDeviceID20>IDTAUGUSTAHID_MONETARY_E2E</SecureDeviceID20>
    <NumPadTypes20>1</NumPadTypes20>
    <SecureDevice20PadType1>None</SecureDevice20PadType1>
  </SecureDevice20>
  <SecureDevice21>
    <Description21>IDTech SecureHead with Monetary E2E Encryption</Description21>
    <Interface21>USB/HID</Interface21>
    <SecureDeviceID21>IDTSECUREHEADHID_MONETARY_E2E</SecureDeviceID21>
    <NumPadTypes21>1</NumPadTypes21>
    <SecureDevice21PadType1>None</SecureDevice21PadType1>
  </SecureDevice21>
  <SecureDevice22>
    <Description22>MagTek IPAD PIN pad with Mercury E2E Encryption</Description22>
    <Interface22>USB/HID</Interface22>
    <SecureDeviceID22>MTIPADHID</SecureDeviceID22>
    <NumPadTypes22>2</NumPadTypes22>
    <SecureDevice22PadType1>IPAD100</SecureDevice22PadType1>
    <SecureDevice22PadType2>None</SecureDevice22PadType2>
  </SecureDevice22>
  <SecureDevice23>
    <Description23>Equinox Payments L5300 PIN pad</Description23>
    <Interface23>RS-232</Interface23>
    <SecureDeviceID23>EQUINOXL5300</SecureDeviceID23>
    <NumPadTypes23>2</NumPadTypes23>
    <SecureDevice23PadType1>L5300</SecureDevice23PadType1>
    <SecureDevice23PadType2>None</SecureDevice23PadType2>
  </SecureDevice23>
  <SecureDevice24>
    <Description24>Equinox Payments L5200 PIN pad</Description24>
    <Interface24>RS-232</Interface24>
    <SecureDeviceID24>EQUINOXL5200</SecureDeviceID24>
    <NumPadTypes24>2</NumPadTypes24>
    <SecureDevice24PadType1>L5200</SecureDevice24PadType1>
    <SecureDevice24PadType2>None</SecureDevice24PadType2>
  </SecureDevice24>
  <SecureDevice25>
    <Description25>J2 650 POS Internal MSR</Description25>
    <Interface25>RS-232</Interface25>
    <SecureDeviceID25>J2650MSRVCOM</SecureDeviceID25>
    <NumPadTypes25>1</NumPadTypes25>
    <SecureDevice25PadType1>None</SecureDevice25PadType1>
  </SecureDevice25>
  <SecureDevice26>
    <Description26>Firich FEC Gladius Smart AL7385 MSR</Description26>
    <Interface26>USB/HID</Interface26>
    <SecureDeviceID26>FECMSRHID</SecureDeviceID26>
    <NumPadTypes26>1</NumPadTypes26>
    <SecureDevice26PadType1>None</SecureDevice26PadType1>
  </SecureDevice26>
  <SecureDevice27>
    <Description27>FEC AER Touch POS Terminal MSR</Description27>
    <Interface27>USB/HID</Interface27>
    <SecureDeviceID27>FECAERMSRHID</SecureDeviceID27>
    <NumPadTypes27>1</NumPadTypes27>
    <SecureDevice27PadType1>None</SecureDevice27PadType1>
  </SecureDevice27>
  <SecureDevice28>
    <Description28>IBM SurePOS 500 MSR</Description28>
    <Interface28>RS-232 (19200 baud)</Interface28>
    <SecureDeviceID28>IBMSUREPOSMSR</SecureDeviceID28>
    <NumPadTypes28>1</NumPadTypes28>
    <SecureDevice28PadType1>None</SecureDevice28PadType1>
  </SecureDevice28>
  <SecureDevice29>
    <Description29>Micros PCWS 2015 System MSR</Description29>
    <Interface29>Internal Micros PCWS</Interface29>
    <SecureDeviceID29>MICROSPCWSMSR</SecureDeviceID29>
    <NumPadTypes29>1</NumPadTypes29>
    <SecureDevice29PadType1>None</SecureDevice29PadType1>
  </SecureDevice29>
  <SecureDevice30>
    <Description30>POSX EVO TP4 All-In-One POS MSR</Description30>
    <Interface30>USB/HID</Interface30>
    <SecureDeviceID30>POSXMSRHID</SecureDeviceID30>
    <NumPadTypes30>1</NumPadTypes30>
    <SecureDevice30PadType1>None</SecureDevice30PadType1>
  </SecureDevice30>
  <SecureDevice31>
    <Description31>AnyShop POS MSR</Description31>
    <Interface31>USB/HID</Interface31>
    <SecureDeviceID31>ANYSHOPPOSMSRHID</SecureDeviceID31>
    <NumPadTypes31>1</NumPadTypes31>
    <SecureDevice31PadType1>None</SecureDevice31PadType1>
  </SecureDevice31>
  <SecureDevice32>
    <Description32>Elo ETT10A1 Tablet MSR</Description32>
    <Interface32>RS-232 or USB/VCom</Interface32>
    <SecureDeviceID32>ELOTABLETMSR</SecureDeviceID32>
    <NumPadTypes32>1</NumPadTypes32>
    <SecureDevice32PadType1>None</SecureDevice32PadType1>
  </SecureDevice32>
  <SecureDevice33>
    <Description33>MSR or Pad on Tran</Description33>
    <Interface33>IP</Interface33>
    <SecureDeviceID33>ONTRAN</SecureDeviceID33>
    <NumPadTypes33>1</NumPadTypes33>
    <SecureDevice33PadType1>None</SecureDevice33PadType1>
  </SecureDevice33>
  <SecureDevice34>
    <Description34>Verifone MX915 PinPad</Description34>
    <Interface34>RS-232</Interface34>
    <SecureDeviceID34>VERIFONEMX915</SecureDeviceID34>
    <NumPadTypes34>2</NumPadTypes34>
    <SecureDevice34PadType1>MX915</SecureDevice34PadType1>
    <SecureDevice34PadType2>None</SecureDevice34PadType2>
  </SecureDevice34>
  <SecureDevice35>
    <Description35>PioneerPOS StealthTouch MSR</Description35>
    <Interface35>USB/HID</Interface35>
    <SecureDeviceID35>PIONEERSTM5MSRHID</SecureDeviceID35>
    <NumPadTypes35>1</NumPadTypes35>
    <SecureDevice35PadType1>None</SecureDevice35PadType1>
  </SecureDevice35>
  <SecureDevice36>
    <Description36>HP Retail Jacket</Description36>
    <Interface36>USB/HID</Interface36>
    <SecureDeviceID36>HPRETAILJACKET</SecureDeviceID36>
    <NumPadTypes36>1</NumPadTypes36>
    <SecureDevice36PadType1>None</SecureDevice36PadType1>
  </SecureDevice36>
  <SecureDevice37>
    <Description37>IDTech SecuRED Encrypted</Description37>
    <Interface37>USB/HID</Interface37>
    <SecureDeviceID37>IDTSECUREDMSRHID</SecureDeviceID37>
    <NumPadTypes37>1</NumPadTypes37>
    <SecureDevice37PadType1>None</SecureDevice37PadType1>
  </SecureDevice37>
  <SecureDevice38>
    <Description38>UIC 795SE PIN pad</Description38>
    <Interface38>RS-232 or USB/VCom</Interface38>
    <SecureDeviceID38>UIC795SE</SecureDeviceID38>
    <NumPadTypes38>1</NumPadTypes38>
    <SecureDevice38PadType1>UIC795SE</SecureDevice38PadType1>
  </SecureDevice38>
  <SecureDevice39>
    <Description39>UIC 795 SE PIN pad with TSYS VOLTAGE</Description39>
    <Interface39>RS-232 or USB/VCom</Interface39>
    <SecureDeviceID39>UIC795SE_TSYS_VOLT</SecureDeviceID39>
    <NumPadTypes39>1</NumPadTypes39>
    <SecureDevice39PadType1>UIC795SE</SecureDevice39PadType1>
  </SecureDevice39>
  <SecureDevice40>
    <Description40>Verifone Vx805 PIN pad running XPI</Description40>
    <Interface40>RS-232 or USB/VCom</Interface40>
    <SecureDeviceID40>VX805XPI</SecureDeviceID40>
    <NumPadTypes40>1</NumPadTypes40>
    <SecureDevice40PadType1>VX805</SecureDevice40PadType1>
  </SecureDevice40>
  <SecureDevice41>
    <Description41>ID Innovations Classic MSR</Description41>
    <Interface41>USB/HID</Interface41>
    <SecureDeviceID41>IDINNOVATIONS_CLASSIC_MSRHID</SecureDeviceID41>
    <NumPadTypes41>1</NumPadTypes41>
    <SecureDevice41PadType1>None</SecureDevice41PadType1>
  </SecureDevice41>
  <SecureDevice42>
    <Description42>ID Innovations Value MSR</Description42>
    <Interface42>USB/HID</Interface42>
    <SecureDeviceID42>IDINNOVATIONS_VALUE_MSRHID</SecureDeviceID42>
    <NumPadTypes42>1</NumPadTypes42>
    <SecureDevice42PadType1>None</SecureDevice42PadType1>
  </SecureDevice42>
  <SecureDevice43>
    <Description43>Verifone MX850 PinPad</Description43>
    <Interface43>RS-232</Interface43>
    <SecureDeviceID43>VERIFONEMX850</SecureDeviceID43>
    <NumPadTypes43>2</NumPadTypes43>
    <SecureDevice43PadType1>MX850</SecureDevice43PadType1>
    <SecureDevice43PadType2>None</SecureDevice43PadType2>
  </SecureDevice43>
  <SecureDevice44>
    <Description44>HP RP77800</Description44>
    <Interface44>USB/HID</Interface44>
    <SecureDeviceID44>HP_RP77800</SecureDeviceID44>
    <NumPadTypes44>1</NumPadTypes44>
    <SecureDevice44PadType1>None</SecureDevice44PadType1>
  </SecureDevice44>
  <SecureDevice45>
    <Description45>Verifone Vx805 Pinpad running XPI with Mercury E2E Encryption</Description45>
    <Interface45>RS-232 or USB/VCom</Interface45>
    <SecureDeviceID45>VX805XPI_MERCURY_E2E</SecureDeviceID45>
    <NumPadTypes45>1</NumPadTypes45>
    <SecureDevice45PadType1>VX805</SecureDevice45PadType1>
  </SecureDevice45>
  <SecureDevice46>
    <Description46>Verifone Vx805 PIN pad running XPI with Contactless</Description46>
    <Interface46>RS-232 or USB/VCom</Interface46>
    <SecureDeviceID46>VX805XPI_CTLS</SecureDeviceID46>
    <NumPadTypes46>1</NumPadTypes46>
    <SecureDevice46PadType1>VX805</SecureDevice46PadType1>
  </SecureDevice46>
  <SecureDevice47>
    <Description47>UIC 215E USB/HID MSR with TSYS VOLTAGE</Description47>
    <Interface47>USB/HID</Interface47>
    <SecureDeviceID47>UICMSR215E_TSYS_VOLT</SecureDeviceID47>
    <NumPadTypes47>1</NumPadTypes47>
    <SecureDevice47PadType1>None</SecureDevice47PadType1>
  </SecureDevice47>
  <SecureDevice48>
    <Description48>APOS MSR USB</Description48>
    <Interface48>USB/HID</Interface48>
    <SecureDeviceID48>APOSMSRHID</SecureDeviceID48>
    <NumPadTypes48>1</NumPadTypes48>
    <SecureDevice48PadType1>None</SecureDevice48PadType1>
  </SecureDevice48>
  <SecureDevice49>
    <Description49>Motion MSR HID - MagTek</Description49>
    <Interface49>USB/HID</Interface49>
    <SecureDeviceID49>MTMOTIONHID</SecureDeviceID49>
    <NumPadTypes49>1</NumPadTypes49>
    <SecureDevice49PadType1>None</SecureDevice49PadType1>
  </SecureDevice49>
  <SecureDevice50>
    <Description50>IDTech - ViVOpay Vend III</Description50>
    <Interface50>RS-232</Interface50>
    <SecureDeviceID50>VIVOVEND3</SecureDeviceID50>
    <NumPadTypes50>1</NumPadTypes50>
    <SecureDevice50PadType1>None</SecureDevice50PadType1>
  </SecureDevice50>
  <SecureDevice51>
    <Description51>Panasonic FZR1 MSR</Description51>
    <Interface51>RS-232/VCOM</Interface51>
    <SecureDeviceID51>PANASONIC_FZR1</SecureDeviceID51>
    <NumPadTypes51>1</NumPadTypes51>
    <SecureDevice51PadType1>None</SecureDevice51PadType1>
  </SecureDevice51>
  <SecureDevice52>
    <Description52>Equinox Payments L5300 PIN pad with Mercury E2E Encryption</Description52>
    <Interface52>RS-232</Interface52>
    <SecureDeviceID52>EQUINOXL5300_MERCURY_E2E</SecureDeviceID52>
    <NumPadTypes52>2</NumPadTypes52>
    <SecureDevice52PadType1>L5300</SecureDevice52PadType1>
    <SecureDevice52PadType2>None</SecureDevice52PadType2>
  </SecureDevice52>
  <SecureDevice53>
    <Description53>Equinox Payments L5200 PIN pad with Mercury E2E Encryption</Description53>
    <Interface53>RS-232</Interface53>
    <SecureDeviceID53>EQUINOXL5200_MERCURY_E2E</SecureDeviceID53>
    <NumPadTypes53>2</NumPadTypes53>
    <SecureDevice53PadType1>L5200</SecureDevice53PadType1>
    <SecureDevice53PadType2>None</SecureDevice53PadType2>
  </SecureDevice53>
  <SecureDevice54>
    <Description54>Verifone Vx805 Pinpad running XPI with Elavon E2E Encryption</Description54>
    <Interface54>RS-232 or USB/VCom</Interface54>
    <SecureDeviceID54>VX805XPI_ELAVON_E2E</SecureDeviceID54>
    <NumPadTypes54>1</NumPadTypes54>
    <SecureDevice54PadType1>VX805</SecureDevice54PadType1>
  </SecureDevice54>
  <SecureDevice55>
    <Description55>Ingenico iSC250 PinPad</Description55>
    <Interface55>RS-232</Interface55>
    <SecureDeviceID55>INGENICOISC250</SecureDeviceID55>
    <NumPadTypes55>2</NumPadTypes55>
    <SecureDevice55PadType1>ISC250</SecureDevice55PadType1>
    <SecureDevice55PadType2>None</SecureDevice55PadType2>
  </SecureDevice55>
  <SecureDevice56>
    <Description56>Ingenico iPP350 PinPad</Description56>
    <Interface56>RS-232</Interface56>
    <SecureDeviceID56>INGENICOIPP350</SecureDeviceID56>
    <NumPadTypes56>2</NumPadTypes56>
    <SecureDevice56PadType1>IPP350</SecureDevice56PadType1>
    <SecureDevice56PadType2>None</SecureDevice56PadType2>
  </SecureDevice56>
  <SecureDevice57>
    <Description57>Ingenico iPP320 PinPad</Description57>
    <Interface57>RS-232</Interface57>
    <SecureDeviceID57>INGENICOIPP320</SecureDeviceID57>
    <NumPadTypes57>2</NumPadTypes57>
    <SecureDevice57PadType1>IPP320</SecureDevice57PadType1>
    <SecureDevice57PadType2>None</SecureDevice57PadType2>
  </SecureDevice57>
  <SecureDevice58>
    <Description58>Ingenico iCMP</Description58>
    <Interface58>RS-232</Interface58>
    <SecureDeviceID58>INGENICOICMP</SecureDeviceID58>
    <NumPadTypes58>2</NumPadTypes58>
    <SecureDevice58PadType1>ICMP</SecureDevice58PadType1>
    <SecureDevice58PadType2>None</SecureDevice58PadType2>
  </SecureDevice58>
  <SecureDevice59>
    <Description59>Ingenico iSC480 PinPad</Description59>
    <Interface59>RS-232</Interface59>
    <SecureDeviceID59>INGENICOISC480</SecureDeviceID59>
    <NumPadTypes59>2</NumPadTypes59>
    <SecureDevice59PadType1>ISC480</SecureDevice59PadType1>
    <SecureDevice59PadType2>None</SecureDevice59PadType2>
  </SecureDevice59>
  <SecureDevice60>
    <Description60>Ingenico iSC250 PinPad with Mercury E2E Encryption</Description60>
    <Interface60>RS-232</Interface60>
    <SecureDeviceID60>INGENICOISC250_MERCURY_E2E</SecureDeviceID60>
    <NumPadTypes60>2</NumPadTypes60>
    <SecureDevice60PadType1>ISC250</SecureDevice60PadType1>
    <SecureDevice60PadType2>None</SecureDevice60PadType2>
  </SecureDevice60>
  <SecureDevice61>
    <Description61>Ingenico iPP350 PinPad with Mercury E2E Encryption</Description61>
    <Interface61>RS-232</Interface61>
    <SecureDeviceID61>INGENICOIPP350_MERCURY_E2E</SecureDeviceID61>
    <NumPadTypes61>2</NumPadTypes61>
    <SecureDevice61PadType1>IPP350</SecureDevice61PadType1>
    <SecureDevice61PadType2>None</SecureDevice61PadType2>
  </SecureDevice61>
  <SecureDevice62>
    <Description62>Ingenico iPP320 PinPad with Mercury E2E Encryption</Description62>
    <Interface62>RS-232</Interface62>
    <SecureDeviceID62>INGENICOIPP320_MERCURY_E2E</SecureDeviceID62>
    <NumPadTypes62>2</NumPadTypes62>
    <SecureDevice62PadType1>IPP320</SecureDevice62PadType1>
    <SecureDevice62PadType2>None</SecureDevice62PadType2>
  </SecureDevice62>
  <SecureDevice63>
    <Description63>Ingenico iCMP PinPad with Mercury E2E Encryption</Description63>
    <Interface63>RS-232</Interface63>
    <SecureDeviceID63>INGENICOICMP_MERCURY_E2E</SecureDeviceID63>
    <NumPadTypes63>2</NumPadTypes63>
    <SecureDevice63PadType1>ICMP</SecureDevice63PadType1>
    <SecureDevice63PadType2>None</SecureDevice63PadType2>
  </SecureDevice63>
  <SecureDevice64>
    <Description64>Ingenico iSC480 PinPad with Mercury E2E Encryption</Description64>
    <Interface64>RS-232</Interface64>
    <SecureDeviceID64>INGENICOISC480_MERCURY_E2E</SecureDeviceID64>
    <NumPadTypes64>2</NumPadTypes64>
    <SecureDevice64PadType1>ISC480</SecureDevice64PadType1>
    <SecureDevice64PadType2>None</SecureDevice64PadType2>
  </SecureDevice64>
  <SecureDevice65>
    <Description65>Ingenico iWL258 PinPad with Mercury E2E Encryption</Description65>
    <Interface65>RS-232</Interface65>
    <SecureDeviceID65>INGENICOIWL258_MERCURY_E2E</SecureDeviceID65>
    <NumPadTypes65>2</NumPadTypes65>
    <SecureDevice65PadType1>IWL250</SecureDevice65PadType1>
    <SecureDevice65PadType2>None</SecureDevice65PadType2>
  </SecureDevice65>
  <SecureDevice66>
    <Description66>Ingenico iSC250 PinPad with Heartland Voltage Encryption</Description66>
    <Interface66>RS-232</Interface66>
    <SecureDeviceID66>INGENICOISC250_HEARTLAND_VOLT</SecureDeviceID66>
    <NumPadTypes66>2</NumPadTypes66>
    <SecureDevice66PadType1>ISC250</SecureDevice66PadType1>
    <SecureDevice66PadType2>None</SecureDevice66PadType2>
  </SecureDevice66>
  <SecureDevice67>
    <Description67>Ingenico iPP350 PinPad with Heartland Voltage Encryption</Description67>
    <Interface67>RS-232</Interface67>
    <SecureDeviceID67>INGENICOIPP350_HEARTLAND_VOLT</SecureDeviceID67>
    <NumPadTypes67>2</NumPadTypes67>
    <SecureDevice67PadType1>IPP350</SecureDevice67PadType1>
    <SecureDevice67PadType2>None</SecureDevice67PadType2>
  </SecureDevice67>
  <SecureDevice68>
    <Description68>Ingenico iPP320 PinPad with Heartland Voltage Encryption</Description68>
    <Interface68>RS-232</Interface68>
    <SecureDeviceID68>INGENICOIPP320_HEARTLAND_VOLT</SecureDeviceID68>
    <NumPadTypes68>2</NumPadTypes68>
    <SecureDevice68PadType1>IPP320</SecureDevice68PadType1>
    <SecureDevice68PadType2>None</SecureDevice68PadType2>
  </SecureDevice68>
  <SecureDevice69>
    <Description69>Ingenico iCMP PinPad with Heartland Voltage Encryption</Description69>
    <Interface69>RS-232</Interface69>
    <SecureDeviceID69>INGENICOICMP_HEARTLAND_VOLT</SecureDeviceID69>
    <NumPadTypes69>2</NumPadTypes69>
    <SecureDevice69PadType1>ICMP</SecureDevice69PadType1>
    <SecureDevice69PadType2>None</SecureDevice69PadType2>
  </SecureDevice69>
  <SecureDevice70>
    <Description70>Ingenico iSC480 PinPad with Heartland Voltage Encryption</Description70>
    <Interface70>RS-232</Interface70>
    <SecureDeviceID70>INGENICOISC480_HEARTLAND_VOLT</SecureDeviceID70>
    <NumPadTypes70>2</NumPadTypes70>
    <SecureDevice70PadType1>ISC480</SecureDevice70PadType1>
    <SecureDevice70PadType2>None</SecureDevice70PadType2>
  </SecureDevice70>
  <SecureDevice71>
    <Description71>Ingenico iSC250 PinPad with TSYS Voltage Encryption</Description71>
    <Interface71>RS-232</Interface71>
    <SecureDeviceID71>INGENICOISC250_TSYS_VOLT</SecureDeviceID71>
    <NumPadTypes71>2</NumPadTypes71>
    <SecureDevice71PadType1>ISC250</SecureDevice71PadType1>
    <SecureDevice71PadType2>None</SecureDevice71PadType2>
  </SecureDevice71>
  <SecureDevice72>
    <Description72>Ingenico iPP350 PinPad with TSYS Voltage Encryption</Description72>
    <Interface72>RS-232</Interface72>
    <SecureDeviceID72>INGENICOIPP350_TSYS_VOLT</SecureDeviceID72>
    <NumPadTypes72>2</NumPadTypes72>
    <SecureDevice72PadType1>IPP350</SecureDevice72PadType1>
    <SecureDevice72PadType2>None</SecureDevice72PadType2>
  </SecureDevice72>
  <SecureDevice73>
    <Description73>Ingenico iPP320 PinPad with TSYS Voltage Encryption</Description73>
    <Interface73>RS-232</Interface73>
    <SecureDeviceID73>INGENICOIPP320_TSYS_VOLT</SecureDeviceID73>
    <NumPadTypes73>2</NumPadTypes73>
    <SecureDevice73PadType1>IPP320</SecureDevice73PadType1>
    <SecureDevice73PadType2>None</SecureDevice73PadType2>
  </SecureDevice73>
  <SecureDevice74>
    <Description74>Ingenico iCMP PinPad with TSYS Voltage Encryption</Description74>
    <Interface74>RS-232</Interface74>
    <SecureDeviceID74>INGENICOICMP_TSYS_VOLT</SecureDeviceID74>
    <NumPadTypes74>2</NumPadTypes74>
    <SecureDevice74PadType1>ICMP</SecureDevice74PadType1>
    <SecureDevice74PadType2>None</SecureDevice74PadType2>
  </SecureDevice74>
  <SecureDevice75>
    <Description75>Ingenico iSC480 PinPad with TSYS Voltage Encryption</Description75>
    <Interface75>RS-232</Interface75>
    <SecureDeviceID75>INGENICOISC480_TSYS_VOLT</SecureDeviceID75>
    <NumPadTypes75>2</NumPadTypes75>
    <SecureDevice75PadType1>ISC480</SecureDevice75PadType1>
    <SecureDevice75PadType2>None</SecureDevice75PadType2>
  </SecureDevice75>
  <SecureDevice76>
    <Description76>Verifone Vx805 (VERSION 12) PIN pad running XPI with Contactless</Description76>
    <Interface76>RS-232 or USB/VCom</Interface76>
    <SecureDeviceID76>VX805XPI_12</SecureDeviceID76>
    <NumPadTypes76>1</NumPadTypes76>
    <SecureDevice76PadType1>VX805</SecureDevice76PadType1>
  </SecureDevice76>
  <SecureDevice77>
    <Description77>Verifone Vx820 (VERSION 12) PIN pad running XPI with Contactless</Description77>
    <Interface77>RS-232 or USB/VCom</Interface77>
    <SecureDeviceID77>VX820XPI_12</SecureDeviceID77>
    <NumPadTypes77>1</NumPadTypes77>
    <SecureDevice77PadType1>VX820</SecureDevice77PadType1>
  </SecureDevice77>
  <SecureDevice78>
    <Description78>Ingenico iSC250 PinPad with RapidConnect DUKPT E2E Encryption</Description78>
    <Interface78>RS-232</Interface78>
    <SecureDeviceID78>INGENICOISC250_RAPIDCONNECT_E2E</SecureDeviceID78>
    <NumPadTypes78>2</NumPadTypes78>
    <SecureDevice78PadType1>ISC250</SecureDevice78PadType1>
    <SecureDevice78PadType2>None</SecureDevice78PadType2>
  </SecureDevice78>
  <SecureDevice79>
    <Description79>Ingenico iPP350 PinPad with RapidConnect DUKPT E2E Encryption</Description79>
    <Interface79>RS-232</Interface79>
    <SecureDeviceID79>INGENICOIPP350_RAPIDCONNECT_E2E</SecureDeviceID79>
    <NumPadTypes79>2</NumPadTypes79>
    <SecureDevice79PadType1>IPP350</SecureDevice79PadType1>
    <SecureDevice79PadType2>None</SecureDevice79PadType2>
  </SecureDevice79>
  <SecureDevice80>
    <Description80>Ingenico iPP320 PinPad with RapidConnect DUKPT E2E Encryption</Description80>
    <Interface80>RS-232</Interface80>
    <SecureDeviceID80>INGENICOIPP320_RAPIDCONNECT_E2E</SecureDeviceID80>
    <NumPadTypes80>2</NumPadTypes80>
    <SecureDevice80PadType1>IPP320</SecureDevice80PadType1>
    <SecureDevice80PadType2>None</SecureDevice80PadType2>
  </SecureDevice80>
  <SecureDevice81>
    <Description81>Ingenico iCMP PinPad with RapidConnect DUKPT E2E Encryption</Description81>
    <Interface81>RS-232</Interface81>
    <SecureDeviceID81>INGENICOICMP_RAPIDCONNECT_E2E</SecureDeviceID81>
    <NumPadTypes81>2</NumPadTypes81>
    <SecureDevice81PadType1>ICMP</SecureDevice81PadType1>
    <SecureDevice81PadType2>None</SecureDevice81PadType2>
  </SecureDevice81>
  <SecureDevice82>
    <Description82>Ingenico iSC480 PinPad with RapidConnect DUKPT E2E Encryption</Description82>
    <Interface82>RS-232</Interface82>
    <SecureDeviceID82>INGENICOISC480_RAPIDCONNECT_E2E</SecureDeviceID82>
    <NumPadTypes82>2</NumPadTypes82>
    <SecureDevice82PadType1>ISC480</SecureDevice82PadType1>
    <SecureDevice82PadType2>None</SecureDevice82PadType2>
  </SecureDevice82>
  <SecureDevice83>
    <Description83>Ingenico iSC250 PinPad with Monetary DUKPT E2E Encryption</Description83>
    <Interface83>RS-232</Interface83>
    <SecureDeviceID83>INGENICOISC250_MONETARY_E2E</SecureDeviceID83>
    <NumPadTypes83>2</NumPadTypes83>
    <SecureDevice83PadType1>ISC250</SecureDevice83PadType1>
    <SecureDevice83PadType2>None</SecureDevice83PadType2>
  </SecureDevice83>
  <SecureDevice84>
    <Description84>Ingenico iPP350 PinPad with Monetary DUKPT E2E Encryption</Description84>
    <Interface84>RS-232</Interface84>
    <SecureDeviceID84>INGENICOIPP350_MONETARY_E2E</SecureDeviceID84>
    <NumPadTypes84>2</NumPadTypes84>
    <SecureDevice84PadType1>IPP350</SecureDevice84PadType1>
    <SecureDevice84PadType2>None</SecureDevice84PadType2>
  </SecureDevice84>
  <SecureDevice85>
    <Description85>Ingenico iPP320 PinPad with Monetary DUKPT E2E Encryption</Description85>
    <Interface85>RS-232</Interface85>
    <SecureDeviceID85>INGENICOIPP320_MONETARY_E2E</SecureDeviceID85>
    <NumPadTypes85>2</NumPadTypes85>
    <SecureDevice85PadType1>IPP320</SecureDevice85PadType1>
    <SecureDevice85PadType2>None</SecureDevice85PadType2>
  </SecureDevice85>
  <SecureDevice86>
    <Description86>Ingenico iCMP PinPad with Monetary DUKPT E2E Encryption</Description86>
    <Interface86>RS-232</Interface86>
    <SecureDeviceID86>INGENICOICMP_MONETARY_E2E</SecureDeviceID86>
    <NumPadTypes86>2</NumPadTypes86>
    <SecureDevice86PadType1>ICMP</SecureDevice86PadType1>
    <SecureDevice86PadType2>None</SecureDevice86PadType2>
  </SecureDevice86>
  <SecureDevice87>
    <Description87>Ingenico iSC480 PinPad with Monetary DUKPT E2E Encryption</Description87>
    <Interface87>RS-232</Interface87>
    <SecureDeviceID87>INGENICOISC480_MONETARY_E2E</SecureDeviceID87>
    <NumPadTypes87>2</NumPadTypes87>
    <SecureDevice87PadType1>ISC480</SecureDevice87PadType1>
    <SecureDevice87PadType2>None</SecureDevice87PadType2>
  </SecureDevice87>
  <SecureDevice88>
    <Description88>Ingenico iSC250 PinPad with Monetary ONGUARD Encryption</Description88>
    <Interface88>RS-232</Interface88>
    <SecureDeviceID88>INGENICOISC250_MONETARY_ONGUARD</SecureDeviceID88>
    <NumPadTypes88>2</NumPadTypes88>
    <SecureDevice88PadType1>ISC250</SecureDevice88PadType1>
    <SecureDevice88PadType2>None</SecureDevice88PadType2>
  </SecureDevice88>
  <SecureDevice89>
    <Description89>Ingenico iPP320 PinPad with Monetary ONGUARD Encryption</Description89>
    <Interface89>RS-232</Interface89>
    <SecureDeviceID89>INGENICOIPP320_MONETARY_ONGUARD</SecureDeviceID89>
    <NumPadTypes89>2</NumPadTypes89>
    <SecureDevice89PadType1>IPP320</SecureDevice89PadType1>
    <SecureDevice89PadType2>None</SecureDevice89PadType2>
  </SecureDevice89>
  <SecureDevice90>
    <Description90>Ingenico iSC250 PinPad with Vantiv DUKPT E2E Encryption</Description90>
    <Interface90>RS-232</Interface90>
    <SecureDeviceID90>INGENICOISC250_VANTIV_E2E</SecureDeviceID90>
    <NumPadTypes90>2</NumPadTypes90>
    <SecureDevice90PadType1>ISC250</SecureDevice90PadType1>
    <SecureDevice90PadType2>None</SecureDevice90PadType2>
  </SecureDevice90>
  <SecureDevice91>
    <Description91>Ingenico iPP350 PinPad with Vantiv DUKPT E2E Encryption</Description91>
    <Interface91>RS-232</Interface91>
    <SecureDeviceID91>INGENICOIPP350_VANTIV_E2E</SecureDeviceID91>
    <NumPadTypes91>2</NumPadTypes91>
    <SecureDevice91PadType1>IPP350</SecureDevice91PadType1>
    <SecureDevice91PadType2>None</SecureDevice91PadType2>
  </SecureDevice91>
  <SecureDevice92>
    <Description92>Ingenico iPP320 PinPad with Vantiv DUKPT E2E Encryption</Description92>
    <Interface92>RS-232</Interface92>
    <SecureDeviceID92>INGENICOIPP320_VANTIV_E2E</SecureDeviceID92>
    <NumPadTypes92>2</NumPadTypes92>
    <SecureDevice92PadType1>IPP320</SecureDevice92PadType1>
    <SecureDevice92PadType2>None</SecureDevice92PadType2>
  </SecureDevice92>
  <SecureDevice93>
    <Description93>Ingenico iCMP PinPad with Vantiv DUKPT E2E Encryption</Description93>
    <Interface93>RS-232</Interface93>
    <SecureDeviceID93>INGENICOICMP_VANTIV_E2E</SecureDeviceID93>
    <NumPadTypes93>2</NumPadTypes93>
    <SecureDevice93PadType1>ICMP</SecureDevice93PadType1>
    <SecureDevice93PadType2>None</SecureDevice93PadType2>
  </SecureDevice93>
  <SecureDevice94>
    <Description94>Ingenico iSC480 PinPad with Vantiv DUKPT E2E Encryption</Description94>
    <Interface94>RS-232</Interface94>
    <SecureDeviceID94>INGENICOISC480_VANTIV_E2E</SecureDeviceID94>
    <NumPadTypes94>2</NumPadTypes94>
    <SecureDevice94PadType1>ISC480</SecureDevice94PadType1>
    <SecureDevice94PadType2>None</SecureDevice94PadType2>
  </SecureDevice94>
  <SecureDevice95>
    <Description95>Ingenico iSC250 PinPad with BluePay DUKPT E2E Encryption</Description95>
    <Interface95>RS-232</Interface95>
    <SecureDeviceID95>INGENICOISC250_BLUEPAY_E2E</SecureDeviceID95>
    <NumPadTypes95>2</NumPadTypes95>
    <SecureDevice95PadType1>ISC250</SecureDevice95PadType1>
    <SecureDevice95PadType2>None</SecureDevice95PadType2>
  </SecureDevice95>
  <SecureDevice96>
    <Description96>Ingenico iPP350 PinPad with BluePay DUKPT E2E Encryption</Description96>
    <Interface96>RS-232</Interface96>
    <SecureDeviceID96>INGENICOIPP350_BLUEPAY_E2E</SecureDeviceID96>
    <NumPadTypes96>2</NumPadTypes96>
    <SecureDevice96PadType1>IPP350</SecureDevice96PadType1>
    <SecureDevice96PadType2>None</SecureDevice96PadType2>
  </SecureDevice96>
  <SecureDevice97>
    <Description97>Ingenico iPP320 PinPad with BluePay DUKPT E2E Encryption</Description97>
    <Interface97>RS-232</Interface97>
    <SecureDeviceID97>INGENICOIPP320_BLUEPAY_E2E</SecureDeviceID97>
    <NumPadTypes97>2</NumPadTypes97>
    <SecureDevice97PadType1>IPP320</SecureDevice97PadType1>
    <SecureDevice97PadType2>None</SecureDevice97PadType2>
  </SecureDevice97>
  <SecureDevice98>
    <Description98>Ingenico iCMP PinPad with BluePay DUKPT E2E Encryption</Description98>
    <Interface98>RS-232</Interface98>
    <SecureDeviceID98>INGENICOICMP_BLUEPAY_E2E</SecureDeviceID98>
    <NumPadTypes98>2</NumPadTypes98>
    <SecureDevice98PadType1>ICMP</SecureDevice98PadType1>
    <SecureDevice98PadType2>None</SecureDevice98PadType2>
  </SecureDevice98>
  <SecureDevice99>
    <Description99>Ingenico iSC480 PinPad with BluePay DUKPT E2E Encryption</Description99>
    <Interface99>RS-232</Interface99>
    <SecureDeviceID99>INGENICOISC480_BLUEPAY_E2E</SecureDeviceID99>
    <NumPadTypes99>2</NumPadTypes99>
    <SecureDevice99PadType1>ISC480</SecureDevice99PadType1>
    <SecureDevice99PadType2>None</SecureDevice99PadType2>
  </SecureDevice99>
  <SecureDevice100>
    <Description100>None</Description100>
    <Interface100>None</Interface100>
    <SecureDeviceID100>NONE</SecureDeviceID100>
    <NumPadTypes100>1</NumPadTypes100>
    <SecureDevice100PadType1>None</SecureDevice100PadType1>
  </SecureDevice100>
</Devices>