/****** Object:  Table [dbo].[TR_Automatico]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TR_Automatico](
	[IDDeAutomatico] [numeric](18, 0) NULL,
	[IDDeCuenta] [numeric](18, 0) NULL,
	[Codigo] [nvarchar](100) NULL,
	[Descripcion] [nvarchar](255) NULL,
	[Tipo] [bit] NULL,
	[Formula] [nvarchar](255) NULL,
	[CentroCosto] [nvarchar](255) NULL,
	[DescripcionMov] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TMP_Comprobante]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TMP_Comprobante](
	[IDDeCuenta] [numeric](18, 0) NULL,
	[Codigo] [nvarchar](50) NULL,
	[Tipo] [smallint] NULL,
	[Monto] [float] NULL,
	[Referencia] [nvarchar](200) NULL,
	[Descripcion] [nvarchar](200) NULL,
	[Fecha] [datetime] NULL,
	[Usuario] [nvarchar](50) NULL,
	[Linea] [numeric](18, 0) NULL,
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[IDDeCentroCosto] [numeric](18, 0) NULL,
	[cs_sucursal] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_TMP_Comprobante] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TemporalSupervisor]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TemporalSupervisor](
	[IDDeDepartamento] [numeric](18, 0) NULL,
	[Codigo] [nvarchar](50) NULL,
	[Descripcion] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MATIPOCUENTAS]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MATIPOCUENTAS](
	[Codigo] [int] NULL,
	[Descripcion] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MA_USUARIOS]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MA_USUARIOS](
	[codusuario] [nvarchar](10) NOT NULL,
	[password] [nvarchar](50) NOT NULL,
	[login_name] [nvarchar](50) NOT NULL,
	[descripcion] [nvarchar](50) NOT NULL,
	[Nivel] [numeric](18, 0) NULL,
	[add_date] [datetime] NULL,
	[update_date] [datetime] NULL,
	[tipo_usuario] [bit] NULL,
	[id] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ma_TipoCategoria]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ma_TipoCategoria](
	[IDDeTipo] [numeric](18, 0) NULL,
	[NombreTipo] [nvarchar](50) NULL,
	[IDDeCategoria] [nvarchar](50) NULL,
	[NombreCategoria] [nvarchar](100) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MA_SYSTABLAS]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MA_SYSTABLAS](
	[TABLA] [nvarchar](50) NOT NULL,
	[CAMPO] [nvarchar](50) NOT NULL,
	[TIPO] [nvarchar](20) NOT NULL,
	[LONGITUD] [int] NOT NULL,
	[VALOR] [nvarchar](50) NOT NULL,
	[NULO] [bit] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MA_SUCURSALES]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MA_SUCURSALES](
	[cs_Codigo] [nvarchar](15) NOT NULL,
	[cs_Descripcion] [nvarchar](50) NOT NULL,
	[cs_Direccion] [nvarchar](50) NOT NULL,
	[cs_Gerente] [nvarchar](50) NOT NULL,
	[cs_SubGerente] [nvarchar](50) NOT NULL,
	[cs_Telefono] [nvarchar](50) NOT NULL,
	[cs_Ciudad] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MA_Saldo_Acumulado]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MA_Saldo_Acumulado](
	[IDDeCuenta] [numeric](10, 0) NULL,
	[Codigo] [nvarchar](100) NULL,
	[MontoAcumulado] [float] NULL,
	[FechaInicializacion] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MA_REGLASDENEGOCIO]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MA_REGLASDENEGOCIO](
	[CAMPO] [nvarchar](50) NOT NULL,
	[VALOR] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_MA_REGLASDENEGOCIO] PRIMARY KEY CLUSTERED 
(
	[CAMPO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MA_Nivel]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MA_Nivel](
	[Nivel] [numeric](18, 0) NOT NULL,
	[Caracteres] [nvarchar](255) NULL,
	[Separador] [nvarchar](5) NULL,
 CONSTRAINT [PK_MA_Nivel] PRIMARY KEY CLUSTERED 
(
	[Nivel] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MA_ERRORES]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MA_ERRORES](
	[NUMERO] [nvarchar](50) NOT NULL,
	[DESCRIPCION] [ntext] NOT NULL,
	[FECHA] [datetime] NOT NULL,
	[HORA] [datetime] NOT NULL,
	[USUARIO] [nvarchar](10) NOT NULL,
	[MODULO] [nvarchar](50) NOT NULL,
	[FORMA] [nvarchar](50) NOT NULL,
	[OBJ_PRO_FUN] [nvarchar](50) NOT NULL,
	[ID] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MA_Empresa]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MA_Empresa](
	[Codigo] [numeric](10, 0) NULL,
	[Nombre] [nvarchar](100) NULL,
	[Direccion] [nvarchar](100) NULL,
	[RIF] [nvarchar](50) NULL,
	[NIT] [nvarchar](50) NULL,
	[Telefonos] [nvarchar](50) NULL,
	[Fax] [nvarchar](50) NULL,
	[Ciudad] [nvarchar](50) NULL,
	[Estado] [nvarchar](50) NULL,
	[Pais] [nvarchar](50) NULL,
	[Contacto] [nvarchar](50) NULL,
	[TelfContacto] [nvarchar](50) NULL,
	[CorreoElectronico] [nvarchar](50) NULL,
	[PaginaWeb] [nvarchar](50) NULL,
	[FechaInicializacion] [datetime] NULL,
	[FechaTerminacion] [datetime] NULL,
	[FechaUltCierre] [nvarchar](50) NULL,
	[Mascara] [nvarchar](50) NULL,
	[AUTOMATICO] [smallint] NULL,
	[SeparadorCuenta] [nvarchar](5) NULL,
	[RepetirLinea] [bit] NOT NULL,
	[CodCuentaEje] [nvarchar](50) NOT NULL,
	[CodCuentaAcu] [nvarchar](50) NOT NULL,
	[bs_bloquearperiodo] [bit] NOT NULL,
	[nu_nivel] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ma_Ejecucion]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ma_Ejecucion](
	[IDDeAutomatico] [numeric](18, 0) NULL,
	[Nombre] [nvarchar](255) NULL,
	[FechaEjecucion] [datetime] NULL,
	[FechaRegistro] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ma_DepCat]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ma_DepCat](
	[IDDeDepCat] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Codigo] [nvarchar](200) NOT NULL,
	[Descripcion] [nvarchar](200) NOT NULL,
	[Relacion] [nvarchar](15) NOT NULL,
	[TipoRel] [nvarchar](15) NOT NULL,
	[Clave] [nvarchar](15) NOT NULL,
	[Texto] [nvarchar](200) NOT NULL,
	[Imagen] [nvarchar](15) NOT NULL,
	[Tag] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Ma_DepCat] PRIMARY KEY CLUSTERED 
(
	[IDDeDepCat] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ma_CuentasActivos]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ma_CuentasActivos](
	[IDDeCategoria] [numeric](18, 0) NOT NULL,
	[CuentaReal] [nvarchar](50) NULL,
	[CuentaGasto] [nvarchar](50) NULL,
	[CuentaAcumulada] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ma_cuentas_distribuir]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ma_cuentas_distribuir](
	[codigo] [nvarchar](5) NOT NULL,
	[descripcion] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ma_cuentas_distribuir] PRIMARY KEY CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MA_Cuentas]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MA_Cuentas](
	[IDDeCuenta] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[Codigo] [nvarchar](200) NOT NULL,
	[Descripcion] [nvarchar](200) NOT NULL,
	[IDDeCategoria] [numeric](10, 0) NOT NULL,
	[Tipo] [int] NULL,
	[Status] [int] NULL,
	[RELACION] [nvarchar](15) NULL,
	[TIPOREL] [nvarchar](15) NULL,
	[CLAVE] [nvarchar](15) NULL,
	[TEXTO] [nvarchar](200) NULL,
	[IMAGEN] [nvarchar](15) NULL,
	[TAG] [nvarchar](50) NULL,
	[NUMERO] [nvarchar](200) NULL,
	[CENTROCOSTO] [bit] NULL,
	[PRESUPUESTO] [bit] NULL,
 CONSTRAINT [PK_MA_Cuentas] PRIMARY KEY CLUSTERED 
(
	[IDDeCuenta] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MA_CorridasAgente]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MA_CorridasAgente](
	[cs_corrida] [nvarchar](15) NOT NULL,
	[ds_fecha] [datetime] NOT NULL,
	[cs_tipocorrida] [nvarchar](15) NOT NULL,
 CONSTRAINT [PK_MA_CorridasAgente] PRIMARY KEY CLUSTERED 
(
	[cs_corrida] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MA_Correlativos]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MA_Correlativos](
	[CU_Campo] [nvarchar](50) NOT NULL,
	[NU_VALOR] [numeric](18, 0) NOT NULL,
	[cu_formato] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_MA_Consecutivos] PRIMARY KEY CLUSTERED 
(
	[CU_Campo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MA_Consecutivos]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MA_Consecutivos](
	[IDDeComprobante] [int] NULL,
	[IDDeCuentas] [int] NULL,
	[IDDeMovimientos] [int] NULL,
	[IDDeActivo] [int] NULL,
	[RELACION] [int] NULL,
	[ORGANIZACION] [numeric](18, 0) NULL,
	[RELACIONDEP] [numeric](18, 0) NULL,
	[RELACIONCEN] [numeric](18, 0) NULL,
	[Promventas] [numeric](18, 0) NULL,
	[Detalleinv] [numeric](18, 0) NULL,
	[Promocion] [numeric](18, 0) NULL,
	[d_moneda] [numeric](18, 0) NULL,
	[codcierre] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MA_ComprobantesAutomaticos]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MA_ComprobantesAutomaticos](
	[cs_documento] [nvarchar](15) NOT NULL,
	[cs_descripcion] [nvarchar](120) NOT NULL,
	[bs_estado] [bit] NOT NULL,
	[bs_Tipo] [bit] NOT NULL,
	[cs_frecuencia_modo] [char](1) NOT NULL,
	[ns_frecuencia_valor] [int] NOT NULL,
	[bs_modo] [bit] NOT NULL,
	[Origen_Documento] [nvarchar](15) NOT NULL,
	[cs_observacion] [ntext] NOT NULL,
 CONSTRAINT [PK_MA_ComprobantesAutomaticos] PRIMARY KEY CLUSTERED 
(
	[cs_documento] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MA_COMPROBANTES_AUT_NOMINA]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MA_COMPROBANTES_AUT_NOMINA](
	[cs_documento] [nvarchar](15) NOT NULL,
	[cs_descripcion] [nvarchar](120) NOT NULL,
	[ns_proceso] [int] NOT NULL,
	[cs_tabla_ma_proceso] [nvarchar](50) NOT NULL,
	[cs_tabla_tr_proceso] [nvarchar](50) NOT NULL,
	[cs_contrapartida] [nvarchar](50) NOT NULL,
	[bs_activo] [bit] NOT NULL,
 CONSTRAINT [PK_MA_COMPROBANTES_AUT_NOMINA] PRIMARY KEY CLUSTERED 
(
	[cs_documento] ASC,
	[ns_proceso] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MA_Comprobantes]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MA_Comprobantes](
	[IDDeComprobante] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[Codigo] [nvarchar](50) NULL,
	[Fecha] [datetime] NOT NULL,
	[Status] [int] NULL,
	[Descripcion] [nvarchar](100) NULL,
	[Observacion] [nvarchar](512) NULL,
	[Cerrado] [int] NULL,
	[Estado] [nvarchar](15) NULL,
	[CODUSUARIO] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_MA_Comprobantes] PRIMARY KEY CLUSTERED 
(
	[IDDeComprobante] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MA_CIERREEJERCICIO_CORRIDAS]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MA_CIERREEJERCICIO_CORRIDAS](
	[cu_codigo] [nvarchar](50) NOT NULL,
	[cu_ano] [numeric](18, 0) NOT NULL,
	[ds_fecha] [datetime] NOT NULL,
	[cu_codusuario] [nvarchar](50) NOT NULL,
	[cu_comprobante] [nvarchar](50) NOT NULL,
	[idcomprobante] [numeric](18, 0) NOT NULL,
	[bu_Anulado] [int] NOT NULL,
 CONSTRAINT [PK_MA_CIERREEJECICIO] PRIMARY KEY CLUSTERED 
(
	[cu_codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MA_CIERREEJERCICIO_CONDICIONES]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MA_CIERREEJERCICIO_CONDICIONES](
	[CU_CODIGO] [nvarchar](50) NOT NULL,
	[CU_DESCRIPCION] [nvarchar](50) NOT NULL,
	[NU_PORCENTAJERESERVA] [float] NOT NULL,
	[NU_PORCENTAJERESERVA_MAXIMO] [float] NOT NULL,
	[NU_IDCUENTADESTINO] [numeric](18, 0) NOT NULL,
	[NU_DIAS_DIFERENCIACOMPROBANTE] [numeric](18, 0) NOT NULL,
	[BU_ACTIVO] [int] NOT NULL,
 CONSTRAINT [PK_MA_CIERREEJECICIO_CONDICIONES] PRIMARY KEY CLUSTERED 
(
	[CU_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MA_CENTROCOSTOxUNIDAD]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MA_CENTROCOSTOxUNIDAD](
	[cs_codunidad] [nvarchar](30) NOT NULL,
	[cs_localidad] [nvarchar](10) NOT NULL,
	[cs_centrocosto] [nvarchar](15) NOT NULL,
 CONSTRAINT [PK_MA_CENTROCOSTOxUNIDAD] PRIMARY KEY CLUSTERED 
(
	[cs_codunidad] ASC,
	[cs_localidad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MA_CentroCosto]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MA_CentroCosto](
	[IDDeCentroCosto] [numeric](18, 0) IDENTITY(0,1) NOT NULL,
	[Codigo] [nvarchar](200) NULL,
	[Descripcion] [nvarchar](200) NULL,
	[RELACION] [nvarchar](15) NULL,
	[TIPOREL] [nvarchar](15) NULL,
	[CLAVE] [nvarchar](15) NULL,
	[TEXTO] [nvarchar](200) NULL,
	[IMAGEN] [nvarchar](15) NULL,
	[TAG] [nvarchar](50) NULL,
	[NUMERO] [nvarchar](50) NULL,
 CONSTRAINT [PK_MA_CentroCosto] PRIMARY KEY CLUSTERED 
(
	[IDDeCentroCosto] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MA_Categorias]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MA_Categorias](
	[IDDeCategoria] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Codigo] [nvarchar](5) NULL,
	[Descripcion] [nvarchar](50) NULL,
	[CuentaReal] [smallint] NULL,
 CONSTRAINT [PK_MA_Categorias] PRIMARY KEY CLUSTERED 
(
	[IDDeCategoria] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MA_Bienes]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MA_Bienes](
	[IDDeActivo] [numeric](10, 0) NOT NULL,
	[Codigo] [nvarchar](15) NOT NULL,
	[Descripcion] [nvarchar](100) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[Monto] [float] NOT NULL,
	[ValorSalvamento] [float] NULL,
	[VidaUtil] [float] NULL,
	[Depreciacion] [float] NULL,
	[Departamento] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MA_AUXILIARES]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MA_AUXILIARES](
	[cs_codigocuenta] [nvarchar](50) NOT NULL,
	[cs_codigo] [nvarchar](50) NOT NULL,
	[cs_tipo] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_MA_AUXILIARES] PRIMARY KEY CLUSTERED 
(
	[cs_codigo] ASC,
	[cs_tipo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ma_Automatico]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ma_Automatico](
	[IDDeAutomatico] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](255) NULL,
	[FechaInicial] [datetime] NOT NULL,
	[Frecuencia1] [numeric](18, 0) NOT NULL,
	[Frecuencia2] [numeric](18, 0) NOT NULL,
	[FechaFinal] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ma_Activos]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ma_Activos](
	[IDDeActivo] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Codigo] [nvarchar](50) NOT NULL,
	[Descripcion] [nvarchar](255) NULL,
	[Serial] [nvarchar](100) NULL,
	[Costo] [numeric](18, 0) NULL,
	[ValorRescate] [numeric](18, 0) NULL,
	[FechaAd] [datetime] NULL,
	[FechaIn] [datetime] NULL,
	[VidaUtil] [numeric](18, 0) NULL,
	[Unidad] [numeric](18, 0) NULL,
	[Tipo] [numeric](18, 0) NULL,
	[IDDeDepartamento] [numeric](18, 0) NULL,
	[IDDeCategoria] [numeric](18, 0) NULL,
	[TipoDep] [numeric](18, 0) NULL,
	[asignadoa] [nvarchar](50) NOT NULL,
	[Color] [nvarchar](50) NOT NULL,
	[Marca] [nvarchar](50) NOT NULL,
	[Estado] [int] NOT NULL,
	[Origen] [int] NOT NULL,
	[imagen] [image] NULL,
	[IDDeClase] [numeric](18, 0) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ESTRUC_SIS]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ESTRUC_SIS](
	[Nom_org] [nvarchar](50) NULL,
	[Dir_org] [nvarchar](255) NULL,
	[tlf_org] [nvarchar](50) NULL,
	[loc_org] [nvarchar](30) NULL,
	[Rif_org] [nvarchar](12) NULL,
	[Nit_org] [nvarchar](12) NULL,
	[Ser_sof] [nvarchar](50) NULL,
	[Key_Sof] [nvarchar](50) NULL,
	[CR_FISCAL] [float] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ESTRUC_ORG]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ESTRUC_ORG](
	[Relacion] [nvarchar](15) NULL,
	[tiporel] [nvarchar](8) NULL,
	[Clave] [nvarchar](15) NULL,
	[texto] [nvarchar](50) NULL,
	[imagen] [nvarchar](10) NULL,
	[tag] [nvarchar](8) NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ESTRUC_MENU]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ESTRUC_MENU](
	[Relacion] [nvarchar](15) NULL,
	[Clave] [nvarchar](15) NOT NULL,
	[texto] [nvarchar](50) NOT NULL,
	[imagen] [nvarchar](50) NOT NULL,
	[tag] [nvarchar](255) NOT NULL,
	[id] [int] IDENTITY(0,1) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Conf_menu_user]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Conf_menu_user](
	[clave_user] [nvarchar](10) NOT NULL,
	[clave_menu] [nvarchar](15) NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TR_CorridasAgente]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TR_CorridasAgente](
	[cs_corrida] [nvarchar](15) NOT NULL,
	[cs_comprobanteautomatico] [nvarchar](15) NOT NULL,
	[cs_comprobantecontable] [nvarchar](15) NOT NULL,
	[ds_fecha] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TR_ComprobantesAutomaticos_Cuenta_Temp]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TR_ComprobantesAutomaticos_Cuenta_Temp](
	[cs_transaccion] [nvarchar](15) NOT NULL,
	[cs_descripcion] [nvarchar](120) NOT NULL,
	[cs_cuenta] [nvarchar](50) NOT NULL,
	[cs_debe] [nvarchar](50) NOT NULL,
	[cs_haber] [nvarchar](50) NOT NULL,
	[cs_campomonto] [nvarchar](50) NOT NULL,
	[bs_modo] [bit] NOT NULL,
	[cs_usuario] [nvarchar](50) NOT NULL,
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[cs_codigocentro] [nvarchar](15) NOT NULL,
	[cs_centro] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TR_ComprobantesAutomaticos_Cuenta]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TR_ComprobantesAutomaticos_Cuenta](
	[cs_documento] [nvarchar](15) NOT NULL,
	[cs_transaccion] [nvarchar](15) NOT NULL,
	[cs_cuenta] [nvarchar](50) NOT NULL,
	[bs_movimiento] [bit] NOT NULL,
	[cs_campomonto] [nvarchar](50) NOT NULL,
	[bs_modo] [bit] NOT NULL,
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[cs_codigocentro] [nvarchar](15) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TR_ComprobantesAutomaticos]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TR_ComprobantesAutomaticos](
	[cs_documento] [nvarchar](15) NOT NULL,
	[cs_transaccion] [nvarchar](15) NOT NULL,
	[cs_localidad] [nvarchar](10) NOT NULL,
	[cs_modulo] [nvarchar](50) NOT NULL,
	[ns_proceso] [numeric](18, 0) NOT NULL,
	[ns_concepto] [numeric](18, 0) NOT NULL,
	[cs_beneficiario] [nvarchar](50) NOT NULL,
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_TR_ComprobantesAutomaticos] PRIMARY KEY CLUSTERED 
(
	[cs_documento] ASC,
	[cs_transaccion] ASC,
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TR_COMPROBANTES_AUT_NOMIMA]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TR_COMPROBANTES_AUT_NOMIMA](
	[CS_DOCUMENTO] [nvarchar](15) NOT NULL,
	[NS_PROCESO] [numeric](18, 0) NOT NULL,
	[CS_CONCEPTO] [nvarchar](50) NOT NULL,
	[CS_CUENTA] [nvarchar](50) NOT NULL,
	[BS_TIPO] [bit] NOT NULL,
	[BS_MODO] [bit] NOT NULL,
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_TR_COMPROBANTES_AUT_NOM] PRIMARY KEY CLUSTERED 
(
	[CS_DOCUMENTO] ASC,
	[NS_PROCESO] ASC,
	[CS_CONCEPTO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 DEBE, 1 HABER' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TR_COMPROBANTES_AUT_NOMIMA', @level2type=N'COLUMN',@level2name=N'BS_TIPO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 RESUMIDO,1 DETALLADO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TR_COMPROBANTES_AUT_NOMIMA', @level2type=N'COLUMN',@level2name=N'BS_MODO'
GO
/****** Object:  Table [dbo].[TR_Comprobantes]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TR_Comprobantes](
	[IDDeMovimiento] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[Codigo] [nvarchar](50) NULL,
	[IDDeCuenta] [numeric](10, 0) NOT NULL,
	[Tipo] [smallint] NULL,
	[Monto] [float] NOT NULL,
	[Referencia] [nvarchar](200) NULL,
	[Descripcion] [nvarchar](255) NULL,
	[Fecha] [datetime] NOT NULL,
	[IDDeComprobante] [numeric](10, 0) NOT NULL,
	[USUARIO] [nvarchar](50) NULL,
	[IDDeCentroCosto] [numeric](18, 0) NULL,
	[sucursal] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_TR_Comprobantes] PRIMARY KEY CLUSTERED 
(
	[IDDeMovimiento] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TR_CIERREEJERCICIO_CORRIDAS_CONDICION]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TR_CIERREEJERCICIO_CORRIDAS_CONDICION](
	[CU_CODIGOCORRIDA] [nvarchar](50) NOT NULL,
	[CU_CODIGO_CONDICIONES] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MA_CIERREEJERCICIO_CONDICIONES_CUENTAS]    Script Date: 08/23/2010 12:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MA_CIERREEJERCICIO_CONDICIONES_CUENTAS](
	[CU_CODIGO] [nvarchar](50) NOT NULL,
	[CU_CUENTA] [nvarchar](50) NOT NULL,
	[CU_IDCUENTA] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
