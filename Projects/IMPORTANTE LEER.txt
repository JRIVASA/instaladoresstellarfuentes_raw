Antes de compilar un nuevo instalador:

1) Colocar la nueva versi�n de la aplicacion
en su carpeta respectiva (\\BIG-DES-01\C:\ARCHIVOS DE PROGRAMA\STELLAR\). NO se le ocurra reemplazar la ruta en el fuente del instalador, trabaje con el orden ya establecido.

2) Acuerdese de ir a Settings y modificar la versi�n de la aplicaci�n para que concuerde con la que se va a incluir en el instalador.

3) Si se necesita incluir un nuevo archivo determinar donde guardarlo para crear la ruta basado en los siguientes criterios:

* DLLs/OCXs compartidos: Son librerias y componentes que pudieran usar otras aplicaciones (no son propias de/para stellar, aunque se necesiten) por lo cual deben incluirse y registrase en (C:\Windows\System32\). Ejemplo: MsFlexGrid.ocx

* DLLs/OCXs propios: Son librerias y componentes que solo usa Stellar y/o otras aplicaciones relacionadas, y deben estar disponibles en la carpeta de instalacion, ya que generalmente se registran y se instancian en el mismo directorio.
Ejemplo: Recsun.dll

*Ejecutables/Aplicaciones: Carpeta de Instalaci�n. Enough said.
Ejemplo: .*exe

*Otros: Carpeta de Instalaci�n. Uso propio de Stellar.
Ejemplo: Bixolon_Forma_Pago.txt  

4) Si no se trata de cambios que afecten la estructura del instalador, compile la versi�n de la edicion reemplazando la version del ejecutable y listo, de otra manera, por favor realice los mismos cambios en el resto de las ediciones de las aplicaciones, si las hubiere.

Ejemplo: Necesito compilar una nueva versi�n de isBusiness Smart Edition. Sigo los pasos anteriores. Obtengo el instalador y sigo los pasos del "Importante Leer" de los instaladores completos.

Ejemplo 2: Necesito compilar una nueva versi�n de isAccount Expert Edition, en el cual se incluye una nueva DLL de un grid o algo asi. Sigo los pasos anteriores. Vuelvo a realizar la misma actualizaci�n con el isACCOUNT Smart y One Edition. Obtengo los 3 instaladores y sigo los pasos del "Importante Leer" de los instaladores completos para la versi�n especifica que necesito.





