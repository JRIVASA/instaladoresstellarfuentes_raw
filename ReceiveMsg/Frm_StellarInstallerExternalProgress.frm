VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form Frm_StellarInstallerExternalProgress 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   2205
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   5880
   ControlBox      =   0   'False
   Icon            =   "Frm_StellarInstallerExternalProgress.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2205
   ScaleWidth      =   5880
   Begin MSComctlLib.ProgressBar Progress 
      Height          =   495
      Left            =   150
      TabIndex        =   3
      Top             =   600
      Width           =   5595
      _ExtentX        =   9869
      _ExtentY        =   873
      _Version        =   393216
      Appearance      =   1
      Scrolling       =   1
   End
   Begin VB.CheckBox chkReplyMessage 
      Caption         =   "Send Reply"
      Height          =   195
      Left            =   5520
      TabIndex        =   2
      Top             =   1200
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   135
   End
   Begin VB.Timer Timer1 
      Left            =   4560
      Top             =   1200
   End
   Begin VB.Label Title 
      Alignment       =   2  'Center
      Caption         =   "Instalaci�n en Curso... Por Favor Espere"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00AE5B00&
      Height          =   495
      Left            =   0
      TabIndex        =   4
      Top             =   120
      Width           =   5895
   End
   Begin VB.Label Process 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   120
      TabIndex        =   1
      Top             =   1560
      Width           =   5655
   End
   Begin VB.Label Label 
      Alignment       =   2  'Center
      Caption         =   "Acci�n en Curso:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   1200
      Width           =   5655
   End
End
Attribute VB_Name = "Frm_StellarInstallerExternalProgress"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" _
    (ByVal lpClassName As String, ByVal lpWindowName As String) As Long

Private Declare Function SetWindowPos Lib "user32" (ByVal hWnd As Long, ByVal hWndInsertAfter As Long, ByVal X As Long, ByVal Y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long

Private FormaCargada As Boolean

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MoverVentana Me.hWnd
End Sub

Private Sub Title_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Form_MouseMove Button, Shift, X, Y
End Sub

Private Sub Label_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Form_MouseMove Button, Shift, X, Y
End Sub

Private Sub Form_Activate()
    
    If Not FormaCargada Then
        
        FormaCargada = True
        
        Dim RawParams As String
        Dim RawParam As String
        Dim Params() As String
        Dim ParamID As String
        
        RawParams = Command$()
        
        Params = Strings.Split(RawParams, "|p|", , vbTextCompare)
        
        ParamID = "CenterWindow=1"
        
        If LocateParam(ParamID, Params, RawParam) Then
            CenterFormVSScreen Me
        End If
        
    End If
    
End Sub

Private Sub Timer1_Timer() '4030E0
    
    Progress.Max = 100
    Progress.Min = 0
    
    If (Progress.Value = 100) Then
        Progress.Value = 0
    Else
        Progress.Value = Progress.Value + 0.5
    End If

End Sub

Public Function LocateParam(ByVal ParamIDString As String, _
ByRef pParamsArr() As String, Optional ByRef OutRawParam) As Boolean
    
    LocateParam = False
    
    Dim RawParam
    
    For Each RawParam In pParamsArr
        
        If UCase(RawParam) Like "*" & UCase(ParamIDString) & "*" Then
            
            If Left(UCase(LTrim(RawParam)), Len(ParamIDString)) = UCase(ParamIDString) Then
                
                LocateParam = True
                
                If Not IsMissing(OutRawParam) Then
                    OutRawParam = RawParam
                End If
                
                Exit Function
                
            End If
            
        End If
        
    Next
    
End Function

Public Function GetParamValue(ByVal ParamIDString As String, ByRef pRawParam As String) As String
    
    Dim ParamIDLength As Long
    
    ParamIDLength = Len(ParamIDString)
    
    If Len(ParamIDString) < Len(LTrim(pRawParam)) Then
        GetParamValue = Strings.Mid(LTrim(pRawParam), Len(ParamIDString) + 1, Len(pRawParam) - Len(ParamIDString) + 1)
    Else
        GetParamValue = vbNullString
    End If
    
End Function

Private Sub Form_Load()
    
    FormaCargada = False
    
    Call LocateForm(Me)
    
    Timer1.Enabled = True
    Timer1.Interval = 1
    
    Load frmInvisible
    
    ' Get this form's handle.
    ' Subclass this form to trap Windows messages
    ' so we know when a message is send from the
    ' sending application.
    
    'lHwnd = Me.hwnd
    
    'Now instead use the whole project handle.
    
    Dim RecAppWindowName As String
    
    RecAppWindowName = "StellarInstallerExternalProgressID"
    lHwnd = FindWindow(vbNullString, RecAppWindowName) 'Me.hwnd
    
    Debug.Print lHwnd
    
    Call pHook
    
    Dim RawParams As String
    Dim RawParam As String
    Dim Params() As String
    Dim ParamID As String
    
    RawParams = Command$()
    
    Params = Strings.Split(RawParams, "|p|", , vbTextCompare)
    
    ParamID = "NoTitle"
    
    If LocateParam(ParamID, Params) Then
        
        Me.Title.Visible = False
        
        Me.Height = Me.Height - Me.Title.Height
        
        Me.Progress.Top = Me.Progress.Top - (Me.Title.Height - Me.Title.Top)
        Me.Label.Top = Me.Label.Top - (Me.Title.Height - Me.Title.Top)
        Me.Process.Top = Me.Process.Top - (Me.Title.Height - Me.Title.Top)
        
    Else
        
        ParamID = "Title="
        
        If LocateParam(ParamID, Params, RawParam) Then
            Title.Caption = GetParamValue(ParamID, RawParam)
        End If
           
    End If
    
    ParamID = "Subtitle="
    
    If LocateParam(ParamID, Params, RawParam) Then
        Label.Caption = GetParamValue(ParamID, RawParam)
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer) '4032B0
    
    Unload frmInvisible
    
    ' Un-subclass the form.
    
    Call pUnhook
    
End Sub

Public Sub LocateForm(Target) '402C30
    
    Call SetWindowPos(Me.hWnd, -1, 0, 0, 0, 0, 1)
    
    Target.Move (Screen.Width - Me.Width) - 23, 23
    
End Sub

Public Sub CenterFormVSScreen(Target) '402C30
    
    Call SetWindowPos(Me.hWnd, -1, 0, 0, 0, 0, 1)
    
    Target.Move ((Screen.Width / 2) - (Me.Width / 2)), ((Screen.Height / 2) - (Me.Height / 2))
    
End Sub

Private Sub Title_Click()
    'Unload Me
End Sub
