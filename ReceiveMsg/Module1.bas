Attribute VB_Name = "Module1"
Option Explicit

Public lpPrevWndProc As Long
Public lHwnd         As Long

Public Const GWL_WNDPROC = (-4)
Public Const WM_COPYDATA = &H4A

Public Type COPYDATASTRUCT
    dwData As Long
    cbData As Long
    lpData As Long
End Type
'
'Copies a block of memory from one location to another.
'
Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" _
   (hpvDest As Any, hpvSource As Any, ByVal cbCopy As Long)

Declare Function CallWindowProc Lib "user32" Alias _
   "CallWindowProcA" (ByVal lpPrevWndFunc As Long, ByVal hWnd As _
   Long, ByVal Msg As Long, ByVal wParam As Long, ByVal lParam As _
   Long) As Long

Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" _
   (ByVal hWnd As Long, ByVal nIndex As Long, ByVal dwNewLong As _
   Long) As Long

Const WM_NCLBUTTONDOWN = &HA1
Const HTCAPTION = 2
 
Private Declare Function SendMessage Lib "user32.dll" _
Alias "SendMessageA" ( _
ByVal hWnd As Long, _
ByVal wMsg As Long, _
ByVal wParam As Long, _
ByVal lParam As Any) As Long
                         
Public Declare Sub ReleaseCapture Lib "user32" ()
                         
Public Sub pHook()
    
    ' Sub class the form to trap for Windows messages.
    
    lpPrevWndProc = SetWindowLong(lHwnd, GWL_WNDPROC, AddressOf fWindowProc)
    
End Sub

Sub pReceiveMsg(lParam As Long)

    On Error GoTo 0

    Dim sString       As String
    Dim l             As Long
    Dim lnumEls       As Long
    Dim bFailed       As Boolean
    Dim pb            As New PropertyBag
    Dim cds           As COPYDATASTRUCT
    Dim buf(1 To 255) As Byte
    
    
    ' Copy the data sent to this application
    ' into a local structure.
    
    Call CopyMemory(cds, ByVal lParam, Len(cds))
    
            Call CopyMemory(buf(1), ByVal cds.lpData, cds.cbData)
            
            ' Convert the ASCII byte array back to a Unicode string.
            
            sString = StrConv(buf, vbUnicode)
            sString = Left$(sString, InStr(1, sString, Chr$(0)) - 1)
            
            ' Display the received string.
            
            If UCase(sString) = UCase("[StellarInstallerExternalProgressID SAFE APP CLOSE]") _
            Or UCase(sString) = UCase("StellarInstallerExternalProgressID SAFE APP CLOSE") Then
                Unload Frm_StellarInstallerExternalProgress
                Exit Sub
                'End
            End If
            
            'If Left(UCase(sString), 7) = UCase("!Title=") Then
                'sString = Mid(sString, 8, Len(sString))
                'Frm_StellarInstallerExternalProgress.Caption = sString
            'Else
                Frm_StellarInstallerExternalProgress.Process = sString
            'End If
                        
    If bFailed Then
        Frm_StellarInstallerExternalProgress.Process = "N/A"
    End If

End Sub

Public Sub pUnhook()
    
    ' Remove the subclassing.
    
    Call SetWindowLong(lHwnd, GWL_WNDPROC, lpPrevWndProc)
    
End Sub

Function fWindowProc(ByVal hw As Long, ByVal uMsg As Long, _
         ByVal wParam As Long, ByVal lParam As Long) As Long
    
    ' This callback routine is called by Windows whenever
    ' a message is sent to this form.  If it is the copy
    ' message call our procedure to receive the message.
    
    If uMsg = WM_COPYDATA Then Call pReceiveMsg(lParam)
    
    ' Call the original window procedure associated with this form.
    
    fWindowProc = CallWindowProc(lpPrevWndProc, hw, uMsg, wParam, lParam)
    
End Function

Public Sub MoverVentana(hWnd As Long)
    
    On Error Resume Next
    
    Dim lngReturnValue As Long
    
    Call ReleaseCapture
    
    lngReturnValue = SendMessage( _
    hWnd, WM_NCLBUTTONDOWN, HTCAPTION, 0&)
    
End Sub
